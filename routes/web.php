<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('admin')->group(function ()
{
    Route::get('/', 'HomeController@index');//localhost:8000/Home

    Route::prefix('profile')->group(function ()
    {
        Route::get('/', 'HomeController@profile')->name('profile');
        Route::post('update', 'HomeController@update_profile');
    });

    Route::prefix('todo-list')->group(function ()
    {
        Route::get('/', 'HomeController@todo_list')->name('todo');
        Route::post('upload', 'HomeController@todo_upload')->name('todo.upload');
        Route::post('api', 'HomeController@api')->name('todo.api');
        Route::post('delete', 'HomeController@delete')->name('todo.delete');
        Route::post('edit', 'HomeController@edit')->name('todo.edit');
        Route::post('update', 'HomeController@todo_update')->name('todo.update');
        Route::post('alert', 'HomeController@alert')->name('todo.alert');
    });

    Route::prefix('resume')->group(function ()
    {
        Route::get('/', 'ResumeController@show_resume');

        Route::post('update', 'ResumeController@updatesummary');

        Route::post('create_education','ResumeController@store_education');
        Route::post('update_education','ResumeController@update_education');
        Route::delete('destroy_education{id}','ResumeController@destroy_education');

        Route::post('create_experience','ResumeController@store_experience');
        Route::post('update_experience','ResumeController@update_experience');
        Route::delete('destroy_experience{id}','ResumeController@destroy_experience');

        Route::post('create_skill','ResumeController@store_skill');
        Route::post('update_skill','ResumeController@update_skill');
        Route::delete('destroy_skill{id}','ResumeController@destroy_skill');
    });

    Route::prefix('message')->group(function ()
    {
        Route::get('/', 'HomeController@show_message');        
    });
});