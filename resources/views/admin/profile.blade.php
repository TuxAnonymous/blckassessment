@extends('layouts.admin')
@section('content')
<!-- Start Edit Profile Modal -->
<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form role="form" action="{{url('/admin/profile/update')}}" method="post" enctype="multipart/form-data">
                        <div class="card-body">
                        @csrf
                            <input type="hidden" name="id" value="{{auth()->user()->id}}">
                            <div class="d-flex flex-row bd-highlight col-md-12">
                                <label  class="col-md-3">Input E-mail</label>
                                <div class="input-group mb-3">
                                    <input type="email" name="email" class="form-control" placeholder="E-Mail" value="{{auth()->user()->email}}">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-user-secret"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-row bd-highlight col-md-12">
                                <label  class="col-md-3">Input Name</label>
                                <div class="input-group mb-3">
                                    <input type="text" name="name" class="form-control text-capitalize" placeholder="Full Name" value="{{auth()->user()->name}}">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-user-secret"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-row bd-highlight col-md-12">
                                <label  class="col-md-3">Input Birth Date</label>
                                <div class="input-group mb-3">
                                    <input type="date" name="birthday" class="form-control" placeholder="Birth Date" value="{{auth()->user()->birthday}}">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-user-secret"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-row bd-highlight col-md-12">
                                <label  class="col-md-3">Input Address</label>
                                <div class="input-group mb-3">
                                    <input type="text" name="address" class="form-control text-capitalize" placeholder="Address" value="{{auth()->user()->address}}">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-user-secret"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-row bd-highlight col-md-12">
                                <label  class="col-md-3">Input Phone Number</label>
                                <div class="input-group mb-3">
                                    <input type="text" name="phone" class="form-control text-capitalize" placeholder="Phone Number" value="{{auth()->user()->phone}}">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-user-secret"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-row bd-highlight col-md-12 mb-3">
                                <label  class="col-md-3">Select Image</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="img" class="custom-file-input profile-image" id="img">
                                        <label class="custom-file-label" for="exampleInputFile"> {{auth()->user()->img}}</label>
                                    </div>
                                </div>
                            </div>
                            <img alt="..." src="{{asset('storage/user/'.auth()->user()->img)}}" class="img-thumbnail mt-2" width="100" height="100">
                            <div class="d-flex flex-row bd-highlight">
                                <div class="col-md"></div>
                                <button type="reset" class="btn btn-secondary btn-md reset">Reset</button> &nbsp
                                <button type="submit" class="btn btn-primary btn-md"><i class="fas fa-cloud-upload-alt"> Save</i></button> &nbsp
                            </div> <br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- End Edit Profile Modal -->
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function ()
    {
        $('#ModalEdit').modal('toggle');
        // Custom File Input
        bsCustomFileInput.init();

        $('.textarea').summernote();

        // function preview image
        function readURL(input)
        {
            if (input.files && input.files[0])
            {
                var reader = new FileReader();                    
                reader.onload = function(e)
                {
                    $(".modal").find("img").attr('src', e.target.result);
                }                    
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $(".profile-image").change(function()
        {
            readURL(this);
        });

        $(".reset").click(function()
        {
            $(".modal").find("img").removeAttr('src');
            $(".modal").find("img").attr('src','{{asset("storage/".auth()->user()->img)}}');
        });
    });
</script>
@endsection